FROM docker:20.10.1
LABEL maintainer="Tom Gilissen tom.gilissen@naturalis.nl"

ENV SCHEDULER_VERSION 1.0
ENV DOCKER_VERSION 20.10.1

RUN set -eu

# user defined crontab rules
COPY crontab.txt /etc/crontabs/root

# scripts needed by scheduled jobs
COPY *.sh /usr/local/bin/
RUN chmod 755 /usr/local/bin/*.sh
WORKDIR /usr/local/bin

# start crond
ENTRYPOINT ["crond"]
CMD ["-c", "/etc/crontabs", "-f", "-L", "/dev/null"]
