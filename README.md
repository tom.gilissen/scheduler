# scheduler

Scheduler is a docker image that you can use for scheduling jobs, including jobs that run in a docker container.


Run the scheduler:
```bash
$ docker container run --name scheduler -d -v /var/run/docker.sock:/var/run/docker.sock registry.gitlab.com/tom.gilissen/scheduler
```

Check if it's up and running:
```bash
$ docker ps -a
```

Inspect the logs:
```bash
$ docker logs scheduler
```
